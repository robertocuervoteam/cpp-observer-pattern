#ifndef SUBJECT_H_
#define SUBJECT_H_
#include "IObserver.h"
#include "ISubject.h"
#include <list>
#include <memory>
#include <iostream>

namespace observer
{

    class Subject : public ISubject
    {

    private:
        std::list<IObserver *> observers;
        std::string message;

    public:
        virtual ~Subject()
        {
            std::cerr << "Goodbye, I was the Subject." << std::endl;
            observers.clear();
        }

        void registerObserver(IObserver *observer) override;
        void unregisterObserver(IObserver *observer) override;
        void notifyObservers() const override;
        void createMessage(const std::string message = "Empty");
    };
} /* namespace observer */

#endif /* SUBJECT_H_ */
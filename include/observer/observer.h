#ifndef OBSERVER_H_
#define OBSERVER_H_
#include "observer/IObserver.h"
#include "observer/ISubject.h"
#include <memory>
#include <string>
#include <iostream>
namespace observer
{
    class Observer : public IObserver
    {
    public:
        Observer(std::shared_ptr<ISubject> const &subject, const std::string observerName);
        virtual ~Observer();
        void update(const std::string &message_from_subject) override;
        void removeMeFromTheList();
        const std::string getMessage() const;

    private:
        void printInfo() const;
        std::shared_ptr<ISubject> subject;
        std::string observerName;
        std::string messageFromSubject;
    };

} // namespace observer
#endif /* OBSERVER_H_ */
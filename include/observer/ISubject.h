#ifndef ISUBJECT_H_
#define ISUBJECT_H_
#include <memory>

namespace observer
{
    class IObserver;
    class ISubject
    {

    public:
        virtual ~ISubject(){};
        virtual void registerObserver(IObserver *observer) = 0;
        virtual void unregisterObserver(IObserver *observer) = 0;
        virtual void notifyObservers() const = 0;
    };
} /* namespace observer */
#endif /* ISUBJECT_H_ */
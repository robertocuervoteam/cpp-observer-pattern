#ifndef IOBSERVER_H_
#define IOBSERVER_H_
#include <string>
namespace observer
{
    class IObserver
    {
    public:
        virtual ~IObserver(){};
        virtual void update(const std::string &message_from_subject) = 0;
    };
} /* namespace observer */
#endif /* IOBSERVER_H_ */
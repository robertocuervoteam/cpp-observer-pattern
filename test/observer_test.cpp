#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "doctest.h"
#include "observer/subject.h"
#include "observer/observer.h"

TEST_SUITE("Observer pattern test suite" * doctest::description("Observer pattern test suite"))
{

  TEST_CASE("Test observer pattern")
  {
    std::shared_ptr<observer::Subject> subject = std::make_shared<observer::Subject>();
    observer::Observer observer1{subject, "observer1"};
    observer::Observer observer2{subject, "observer2"};
    observer::Observer observer3{subject, "observer3"};
    observer::IObserver *observer4 = new observer::Observer(subject, "observer4");
    std::string message1{"first message"};
    subject->createMessage(message1);
    CHECK(observer1.getMessage() == message1);
    CHECK(observer2.getMessage() == message1);
    CHECK(observer3.getMessage() == message1);
    observer2.removeMeFromTheList();
    std::string message2{"second message"};
    subject->createMessage(message2);
    CHECK(observer1.getMessage() == message2);
    CHECK(observer2.getMessage() == message1);
    CHECK(observer3.getMessage() == message2);
    delete observer4;
  }
  TEST_CASE("Test smoke test observer")
  {
    INFO("this is relevant to all asserts, and here is some var: ");
    std::shared_ptr<observer::Subject> subject = std::make_shared<observer::Subject>();
    observer::Observer observer1{subject, "observer1"};
    CHECK(observer1.getMessage() == "none");
  }
}

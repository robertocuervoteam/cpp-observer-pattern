#include "observer/subject.h"
#include <algorithm>
#include <ios>
namespace observer
{

    void Subject::registerObserver(IObserver *observer)
    {
        this->observers.push_back(observer);
    }
    void Subject::unregisterObserver(IObserver *observer)
    {
        std::cerr << "Observers count is: " << this->observers.size() << std::endl;
        this->observers.remove(observer);
        std::cerr << "Observers count is now: " << this->observers.size() << std::endl;
    }
    void Subject::notifyObservers() const
    {
        std::for_each(observers.begin(), observers.end(), [&](auto &observer) { observer->update(message); });
    }
    void Subject::createMessage(const std::string message)
    {
        this->message = message;
        this->notifyObservers();
    }
} /* namespace observer */
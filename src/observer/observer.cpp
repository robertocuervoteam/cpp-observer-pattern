#include "observer/observer.h"
#include <memory>
namespace observer
{
    
    Observer::Observer(std::shared_ptr<ISubject> const &subject, const std::string observerName) : subject(subject), observerName(observerName), messageFromSubject("none")
    {
        this->subject->registerObserver(this);
    }
    Observer::~Observer()
    {
        std::cerr << "Goodbye, I was Observer: " << this->observerName << std::endl;
    }

    void Observer::update(const std::string &message_from_subject)
    {
        this->messageFromSubject = message_from_subject;
        this->printInfo();
    }
    void Observer::printInfo() const
    {
        std::cerr << this->observerName << ": a new message is available --> " << this->messageFromSubject << std::endl;
    }

    void Observer::removeMeFromTheList()
    {
        this->subject->unregisterObserver(this);
        std::cerr << "Observer \"" << this->observerName << "\" removed from the list" << std::endl;
    }

    const std::string Observer::getMessage() const
    {
        return this->messageFromSubject;
    }

} // namespace observer
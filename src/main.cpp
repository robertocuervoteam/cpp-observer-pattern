#include "observer/subject.h"
#include "observer/observer.h"
#include <iostream>

int main()
{
    std::shared_ptr<observer::Subject> subject = std::make_shared<observer::Subject>();
    observer::Observer observer1{subject, "observer1"};
    observer::Observer observer2{subject, "observer2"};
    observer::Observer observer3{subject, "observer3"};
    subject->createMessage("first message");
    observer2.removeMeFromTheList();
    subject->createMessage("second message");
}
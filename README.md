Observer pattern exercise
========================
Implementation of the Observer pattern with C++17 building with Meson.

This project consists of:

- a library (let's name it `observer`)
- an executable `observerpattern` using that `observer`
- unit test executable for `observer` using doctest


How to build the example
========================

```bash
meson build .
cd build
ninja
ninja test # to run unit tests
```

How to test
===========

```bash
ninja test
```
How to get code coverage
========================
Note that you need `lcov`.
If you already have execute `meson build` you must first delete the `build/` directory.
Then execute again:
```bash
meson build -Db_coverage=true
cd build
ninja test
ninja coverage -C build
```
Html coverage report can be found at file:/cpp-observer-pattern/build/meson-logs/coveragereport/index.html

How to install
==============

In default system directory
---------------------------
```bash
ninja install
```

In specific directory, e.g: ./install
-------------------------------------

```bash
DESTDIR=./install ninja install
```
Running `observerpattern` is a little bit more difficult in this case because the installation directory might not belong to library path (`LD_LIBRARY_PATH`). 

```bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./install/usr/local/lib
./install/usr/local/bin/observerpattern #path to installed binary
```
How to run it
==============
After:
```bash
cd build
ninja
```
You can run `./src/observerpattern`
Or: 
After `ninja install` you can run it just with command `observerpattern`.